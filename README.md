# DOCKER HELLO WORLD in Python

* Criamos o Dockerfile com os seguintes parametros: 

FROM python:3 # para baixar a imagem do python versão 3 mais atual

COPY api.py / #Copiar o código python para a raiz / 

COPY requirements.txt /  #Copiar o requirements para o diretorio / (raiz) 

RUN pip install -r requirements.txt # Forma muito eficaz de instalar com os requirements, com isso toda dependencia vai ser instalada

CMD [ "python", "-u", "api.py" ] # Executar o comando python para executar o código api.py


# Proximo passo é construir a Imagem a partir do Dockerfile, entrar dentro do diretorio app e executar o seguinte comando : 

docker build -t docker-hello-world .

# Após gerar a Imagem vamos rodar o comando para subir o container a partir da imagem gerada

docker run -dit --name docker-helloworld -p 5000:5000 docker-hello-world:latest # estou executando de forma interativa no background expondo a porta 5000 externamente para o acesso. 

# Após rodar este comando, devemos executar o seguinte comando para verificar se a o container foi executado

docker ps -a

# Após isso o teste (manual) seria executar um curl -XGET http://localhost:5000 ou testar via Browser.

http://localhost:5000


#Fim do teste

